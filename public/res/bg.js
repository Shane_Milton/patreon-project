// Define our variables
var patrons = JSON.parse($("#patrons").text()),
    keys = Object.keys(patrons),
    tab = $("#tab"),
    i = -1;

function nextPatron() {
    if((i + 1) <= (keys.length - 1)){
        i += 1;
        updatePatron();
    }
}

function previousPatron() {
    if((i - 1) >= 0){
        i -= 1;
        updatePatron();
    }
}

function updatePatron() {
    tab.find('h1').html("<span class='profilePic' style='background-image:url(" + patrons[keys[i]][2] + ")'></span>" + patrons[keys[i]][0]);
    tab.find('h2').html("$" + (patrons[keys[i]][1] / 100) + "/month &mdash; Singularity Member");
    $(".planchettes .active").removeClass("active");
    $("#planchette"+i).addClass("active");
}

function drawUserPlanchette(){
    for(var p in keys){
        var item = $("<planchette-item/>",{
            id:"planchette"+p,
            data:{count:p},
            click: function(e){
                i = Number($(e.currentTarget).data("count"));
                i -= 1;
                updatePatron();
            }
        }).appendTo(".planchettes");
        
    }
}

nextPatron();
drawUserPlanchette();

$("body").on("contextmenu",function(e){
    e.preventDefault();
    previousPatron();
})