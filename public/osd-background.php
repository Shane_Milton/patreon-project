<?php
    // if($_SERVER['REMOTE_IP'])
    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);
?>
<html>
    <body onclick='nextPatron()'>
        <style>
            
            
            @import url('https://fonts.googleapis.com/css?family=Lato|Raleway:800');
            @import url("/res/he-palette.css");
            body{
                height:100vh;
                width:100vw;
                overflow:hidden;
                font-family:"Lato";
                background-color: var(--he-cornflower);
            }
            h1,h2,h3,h4,h5{
                font-family:"Raleway";
            }
            #tab{
                padding:25px;
                border-radius:10px;
                box-shadow:0 0 30px rgba(0,0,0,.4);
                width:500px;
                position:absolute;
                right:10%;
                top:calc(50vh - 90px);
                background-color: var(--he-white);
            }
            .broughtToYouBy{
                font-style: italic;
                text-align:center;
            }
            .profilePic{
                background-size: cover;
                border-radius: 50%;
                background-color:gray;
                display: inline-block;
                height: 50px;
                width: 50px;
                vertical-align: middle;
                margin-right: 20px;
            }
            tlg-rotate{
                height:150vh;
                width:150vw;
                background-color:var(--he-scarlet);
                overflow:hidden;
                transform: rotate(-45deg) translateY(-77%);
                display:block;
                cursor:none;
            }
            tlg-row{
                display:block;
                animation:rowScroll 30s infinite linear;
                white-space: nowrap;
            }
            tlg-row:nth-of-type(odd){
                margin: 90px -190px 90px;
            }
            @keyframes rowScroll{
                0%{
                    transform:translateX(0px);
                }
                100%{
                    transform:translateX(-270px);
                }
            }
            tlg-logo{
                /*! margin: 40px; */
                height: 90px;
                width:  270px;
                background-image: url("/res/tlg-new.png");
                background-position: center;
                background-repeat: no-repeat;
                background-size: contain;
                display:inline-block;
                opacity:.6;
            }
            .planchettes{
                text-align:center;
            }
            planchette-item{
                display:inline-block;
                height:5px;
                width:5px;
                border-radius:50%;
                margin:5px;
                background-color:rgba(0,0,0,.3);
                cursor: pointer;
            }
            planchette-item.active{
                background-color:black;
            }
        
        
        </style>
        <tlg-rotate>
        <?php
        include "../private/vendor/autoload.php";
        include "../private/patron.php";
        function row($count){
            $logo = "<tlg-logo></tlg-logo>";
            $row = "<tlg-row style='animation-duration:".rand(15,30)."s;'>";
            for($i = 0; $i <= $count; $i++){
                $row .= $logo;
            }
            echo $row . "</tlg-row>";
        }
        for($i = 0; $i <= 16; $i++){
            row(15);
        }
        echo "<script id='patrons' type='text/json'>" . json_encode($fullListOfPatrons['patron32'],JSON_PRETTY_PRINT) . "</script>";
        $count = 0;
        foreach($fullListOfPatrons as $patrons){
            $count += count($patrons);
        }
        echo "<script id='count' type='text/json'></script>";
        ?>
        </tlg-rotate>
        <?php
            echo "<div id='tab'>
                <div class='broughtToYouBy'>This episode of TLG brought to you by <b>$count</b> amazing patrons and...</div>
                <h1></h1>
                <h2></h2>
                <div class='planchettes'></div>
            </div>";
        ?>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src='/res/bg.js'></script>
    </body>
</html>